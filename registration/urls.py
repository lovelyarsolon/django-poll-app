from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views


""" the following are the url patterns (for different views for the app 'polls' """
app_name = 'registration'
urlpatterns = [
    path('', views.home, name='home'),
    path('', include('django.contrib.auth.urls')),
    path('signup/', views.signup, name='signup'),
]