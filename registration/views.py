from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from .forms import LoginForm
from django.urls import reverse_lazy


def home(request):
    return render(request, "registration/home.html")


def signup(request):
    if request.method == 'POST':
        f = UserCreationForm(request.POST)
        if f.is_valid():
            f.save()
            username = f.cleaned_data.get('username')
            raw_password = f.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('polls:index')
            # return HttpResponseRedirect("/polls/")
    else:
        f = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': f})

