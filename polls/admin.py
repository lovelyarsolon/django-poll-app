from django.contrib import admin
from .models import Question, Choice


""" changes the appearance of the form for admin, publication date comes first before the question """

""" in TabularInline format is table-based format so it is easy to edit choices and it does not take so much sceen spaces unlike
    the StackedInline format """


class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


"""  the first element is the title of the fieldset """


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']


""" register models for them to be added in the admin site """


admin.site.register(Question, QuestionAdmin)
