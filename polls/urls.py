from django.urls import path

from . import views


""" the following are the url patterns (for different viewa for the app 'polls' """
app_name = 'polls'
urlpatterns = [
    # changed for Django's generic views
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
